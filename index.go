package main

import(
    //"database/sql"

  	"github.com/labstack/echo"
    _ "github.com/mattn/go-sqlite3"
)

func main(){
  e := echo.New()

  e.GET("/city", func(c echo.Context) error { return c.JSON(200, "GET city") })
  e.PUT("/city", func(c echo.Context) error { return c.JSON(200, "PUT city") })
  e.DELETE("/city/:id", func(c echo.Context) error { return c.JSON(200, "DELETE city "+c.Param("id")) })
}
